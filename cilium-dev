#!/bin/bash

#TODO: merge more of the scripts from cilium/ into this file

cmd="$1"

run_test() {
  test_file="$1"
  echo "# Will run test $test_file"
  sudo -E env PATH="${PATH}" ./$test_file
}

draft_weekly_notes() {
  git log --since="1 week" --author=Alemayhu --pretty=oneline --all
}

run_bisection () {
  checker="$@"
  echo "Starting bisection (assuming bad start)"
  echo "checks performed via '$checker'"
  while true; do
    if eval "$checker"; then
      echo `tput setaf 2`GOOD`tput sgr0`:`git ll`
      if [ -z "$REVERSE" ]; then
        break
      fi
    else
      echo `tput setaf 1`BAD`tput sgr0`:`git ll`
    fi
    # Prevent rerunning initial commit
    if ! git checkout HEAD^1; then
      break
    fi
  done
}

vagrant_email_results() {
  export VAGRANT_LOG=debug

  REPO=""

  if [[ "$1" == "clone" ]]; then
    REPO=`mktemp`
    rm $REPO && mkdir $REPO
    git clone https://github.com/cilium/cilium $REPO
    # TODO: check for a local cache, and git fetch then copy.
    cd $REPO
  fi

  BUILD_LOG="`mktemp`.build.txt"
  RELOAD=1 ./contrib/vagrant/start.sh >> $BUILD_LOG 2>&1
  RUN_TEST_SUITE=1 ./contrib/vagrant/start.sh >> $BUILD_LOG 2>&1
  TEST_STATUS=$([ "$?" == 0 ] && echo "success" || echo "failure")

  if [[ "$2" == "cleanup" ]]; then
    vagrant destroy -f >> $BUILD_LOG 2>&1
    cd -
    rm -rvf $REPO >> $BUILD_LOG 2>&1
  fi

  EMAIL="`mktemp`.email.txt"
  REV="$(git --no-pager log -1 --pretty='tformat:%h (%s, %ad)' --date=short)"
  SUBJECT="BUILD $TEST_STATUS: [cilium]: $REV"
  DATE="`date`"
  FROM="Alexander Alemayhu <alexander@alemayhu.com>"

  echo "From: $FROM" > $EMAIL
  echo "Date: $DATE" >> $EMAIL
  echo "Subject: $SUBJECT" >> $EMAIL

  echo "PWD=`pwd`" >> $EMAIL
  echo "" >> $EMAIL
  echo "" >> $EMAIL
  cat $BUILD_LOG >> $EMAIL

  git send-email --no-validate --to alexander@covalent.io --suppress-cc=all $EMAIL
}

build_docs() {
  check_for()
  {
    if [ ! -f $1 ]; then
      echo "error: did not detect $1"
      echo "Please run in the Documentation directory!"
      exit
    fi
  }

  OUTPUT_DIR=~/src/github.com/scanf/alemayhu.com
  PDF_OUTPUT=c.pdf
  HTML_OUTPUT=cilium-staging

  if [ ! -f conf.py ]; then
    cd Documentation
  fi

  check_for conf.py
  make clean

  make latexpdf
  cp `find . -name '*.pdf'` $OUTPUT_DIR/$PDF_OUTPUT

  make html
  rsync -r _build/html/ $OUTPUT_DIR/$HTML_OUTPUT

  #TODO: add support for pull request <cilium-dev documentation pr=1810>?
  echo PDF output can be found at $OUTPUT_DIR/$PDF_OUTPUT
  echo HTML output can be found at $OUTPUT_DIR/$HTML_OUTPUT

  echo WEB :
  echo https://alemayhu.com/$PDF_OUTPUT
  echo https://alemayhu.com/$HTML_OUTPUT
}

if [[ "$1" == "run_test" ]]; then
  run_test "$2"
elif [[ "$1" == "run_runtime_tests" ]]; then
  sudo -E env PATH="${PATH}" make -C ${GOPATH}/src/github.com/cilium/cilium runtime-tests
elif [[ "$1" == "weekly_notes" ]]; then
  draft_weekly_notes
elif [[ "$1" == "bisect" ]]; then
  shift
  run_bisection "$@"
elif [[ "$1" == "vm_email_tests" ]]; then
  shift
  vagrant_email_results
elif [[ "$1" == "documentation" ]]; then
  build_docs
else
  echo "unsupported options '$@'"
  # TODO: print usage
fi
