#!/bin/bash

set -x
set -e

usage()
{
  echo "linux-dev - small script to help with common kernel tasks"
  echo
  echo "|------------------+-------------------------------------------------|"
  echo "| arg              |                                                 |"
  echo "|------------------+-------------------------------------------------|"
  echo "|                  | No argument equals build and install the kernel |"
  echo "| build            | Only build the kernel                           |"
  echo "| install          | Only install the kernel.                        |"
  echo "| remove <VERSION> | Clean up a previously installed kernel.         |"
  echo "| checkpatch       | Run checkpatch.pl on *.{c, h} files in dir      |"
  echo "| download         | Download a kernel from https://kernel.org       |"
  echo "| ToT              | Reset git repository to use origin/master       |"
  echo "| list             | List available kernel versions on kernel.org    |"
  echo "| export           | Archive ToT                                     |"
  echo "| emaild           | Create emails between HEAD and upstream 	     |"
  echo "|------------------+-------------------------------------------------|"
}

check_for()
{
  if [ ! -f $1 ]; then
    echo "fatal: did not detect $1"
    echo "Please run in the top level kernel directory!"
    exit
  fi
}

build_prepare()
{
    make clean
    echo "INFO: expecting only one config file in /boot/"
    check_for .config
    make olddefconfig
}

build()
{
    make -j`grep -Pc '^processor\t' /proc/cpuinfo`
    make modules
}

install_kernel()
{
    sudo make modules_install
    sudo make install
    sudo make headers_install INSTALL_HDR_PATH=/usr
}

uninstall_kernel()
{
  KERNEL_VERSION=$1
  if [ "$KERNEL_VERSION" == "" ] ; then
    echo "please supply a kernel version, e.g. 4.9.0 or any of the other ones in:"
    ls /boot/vmlinuz-*
    exit
  fi
  sudo rm /boot/*$KERNEL_VERSION*
  sudo rm -rvf /lib/modules/*$KERNEL_VERSION
  # cd /usr/src/kernels/linux-$KERNEL_VERSION
  # make clean
  #  rm -rvf /usr/src/kernels/linux-$KERNEL_VERSION
}

update_grub()
{
  if [ -f /etc/fedora-release ]; then
    local EFI_GRUB_CONFIG=/boot/efi/EFI/fedora/grub.cfg
    local GRUB_CONFIG=/boot/grub2/grub.cfg

    if [ -f $EFI_GRUB_CONFIG ]; then
      sudo grub2-mkconfig -o $EFI_GRUB_CONFIG
    fi
    if [ -f $GRUB_CONFIG ]; then
      sudo grub2-mkconfig -o $GRUB_CONFIG
    fi
  elif [ -f /etc/debian-release ]; then
    sudo update-grub
  else
    echo "You might want to update the grub configuration!"
  fi
}

checkpatch()
{
  set +x
  set +e
  for file in `find . -name '*.c'`;
  do
    $HOME/src/kernel.org/davem/net-next/scripts/checkpatch.pl --file --terse $file
  done
  for file in `find . -name '*.h'`;
  do
    $HOME/src/kernel.org/davem/net-next/scripts/checkpatch.pl --file --terse $file
  done
}

download_kernel()
{
  version=$1
  if [[ $version == "" ]]; then
    echo "please supply a version"
    exit
  fi

  download_url="https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$version.tar.xz"
  if [[ $version == *"rc"* ]]; then
    download_url="https://git.kernel.org/torvalds/t/linux-$version.tar.gz"
  fi

  wget $download_url
}

email_diff_upstream()
 {
   PATCHES_DIR=outgoing
   if ! test -d ${PATCHES_DIR}; then
     # TODO: --cover-letter option
     git format-patch -M 'HEAD..upstream/master' -o ${PATCHES_DIR}/;
   fi

   # TODO: edit step vim $(patches_dir)/0000-*
   echo "git send-email --to myaddr --suppress-cc=all ${PATCHES_DIR}/*"
}

upstream_reset()
{
  git fetch --all
  git push --tags

  email_diff_upstream

  git stash
  git checkout master
  git reset --hard upstream/master
  git push origin master
}

if [[ "remove" == $1 ]]; then
  uninstall_kernel $2
elif [[ "upstream" == $1 ]]; then
  upstream_reset
  exit
elif [[ "help" == $1 ]] || [[ "?" == $1 ]] ; then
  set +x
  set +e
  usage
  exit
elif [[ "checkpatch" == $1 ]]; then
  checkpatch
  exit
elif [[ "download" == $1 ]]; then
  download_kernel $2
  exit
elif [[ "ToT" == $1 ]]; then
  git stash
  git fetch --all
  git reset --hard origin/master
  exit
elif [[ "list" == $1 ]]; then
  links -dump https://kernel.org|grep pgp| awk ' { print $1$2" ("$3")" }'
  exit
elif [[ "export" == $1 ]]; then
  path=/usr/src/kernels/scanf/`git describe --tags`
  mkdir -pv $path
  git checkout-index -a -f --prefix=$path/
  exit
elif [[ "emaild" == $1 ]]; then
  email_diff_upstream
  exit
else
  check_for Kconfig

  if [[ "install" == $1 ]]; then
    echo skipping build
  else
    build_prepare
    build
  fi

  if [[ "build" == $1 ]]; then
    echo skipping install
    exit
  else
    install_kernel
  fi
fi

update_grub
